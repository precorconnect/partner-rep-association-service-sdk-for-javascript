Feature: List partner rep associations
  Lists all partner rep associations with the provided partnerId

  Scenario: Success
    Given I provide a valid accessToken
    When I execute listPartnerRepAssociationsWithPartnerId
    Then all partner rep associations in the partner-rep-association-service with the provided partnerId are returned