import {Container} from 'aurelia-dependency-injection';
import {HttpClient} from 'aurelia-http-client';
import PartnerRepAssociationServiceSdkConfig from './partnerRepAssociationServiceSdkConfig';
import ListPartnerRepAssociationsWithPartnerIdFeature from './listPartnerRepAssociationsWithPartnerIdFeature';

/**
 * @class {DiContainer}
 */
export default class DiContainer {

    _container:Container;

    /**
     * @param {PartnerRepAssociationServiceSdkConfig} config
     */
    constructor(config:PartnerRepAssociationServiceSdkConfig) {

        if (!config) {
            throw 'config required';
        }

        this._container = new Container();

        this._container.registerInstance(PartnerRepAssociationServiceSdkConfig, config);
        this._container.autoRegister(HttpClient);

        this._registerFeatures();

    }

    /**
     * Resolves a single instance based on the provided key.
     * @param key The key that identifies the object to resolve.
     * @return Returns the resolved instance.
     */
    get(key:any):any {
        return this._container.get(key);
    }

    _registerFeatures() {

        this._container.autoRegister(ListPartnerRepAssociationsWithPartnerIdFeature);

    }

}
