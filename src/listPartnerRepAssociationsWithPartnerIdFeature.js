import {inject} from 'aurelia-dependency-injection';
import PartnerRepAssociationServiceSdkConfig from './partnerRepAssociationServiceSdkConfig';
import {HttpClient} from 'aurelia-http-client'
import PartnerRepAssociationSynopsisView from './partnerRepAssociationSynopsisView';

@inject(PartnerRepAssociationServiceSdkConfig, HttpClient)
class ListPartnerRepAssociationsWithPartnerIdFeature {

    _config:PartnerRepAssociationServiceSdkConfig;

    _httpClient:HttpClient;

    constructor(config:PartnerRepAssociationServiceSdkConfig,
                httpClient:HttpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;
    }

    /**
     * Lists all partner rep associations with the provided partnerId
     * @param {string} partnerId
     * @param {string} accessToken
     * @returns a promise of {PartnerRepAssociationSynopsisView[]}
     */
    execute(partnerId:string,
            accessToken:string):Promise<PartnerRepAssociationSynopsisView[]> {

        return this._httpClient
            .createRequest('partner-rep-associations')
            .asGet()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .withParams({
                partnerId: partnerId
            })
            .send()
            .then(response => Array.from(
                response.content,
                contentItem =>
                    new PartnerRepAssociationSynopsisView(
                        contentItem.id,
                        contentItem.partnerId,
                        contentItem.repId
                    )
                )
            );
    }
}

export default ListPartnerRepAssociationsWithPartnerIdFeature;
