/**
 * @class {PartnerRepAssociationSynopsisView}
 */
export default class PartnerRepAssociationSynopsisView {

    _id:string;

    _partnerId:string;

    _repId:string;

    /**
     * @param {string} id
     * @param {string} partnerId
     * @param {string} repId
     */
    constructor(id,
                partnerId,
                repId) {

        if (!id) {
            throw new TypeError('id required');
        }
        this._id = id;

        if (!partnerId) {
            throw new TypeError('partnerId required');
        }
        this._partnerId = partnerId;

        if (!repId) {
            throw new TypeError('repId required');
        }
        this._repId = repId;

    }

    /**
     * @returns {string}
     */
    get id():string {
        return this._id;
    }

    /**
     * @returns {string}
     */
    get partnerId():string {
        return this._partnerId;
    }

    /**
     *
     * @returns {string}
     */
    get repId():string {
        return this._repId;
    }

}