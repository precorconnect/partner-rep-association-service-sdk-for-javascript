import PartnerRepAssociationServiceSdkConfig from './partnerRepAssociationServiceSdkConfig';
import DiContainer from './diContainer';
import PartnerRepAssociationSynopsisView from './partnerRepAssociationSynopsisView';
import ListPartnerRepAssociationsWithPartnerIdFeature from './listPartnerRepAssociationsWithPartnerIdFeature';

/**
 * @class {PartnerRepAssociationServiceSdk}
 */
export default class PartnerRepAssociationServiceSdk {

    _diContainer:DiContainer;

    /**
     * @param {PartnerRepAssociationServiceSdkConfig} config
     */
    constructor(config:PartnerRepAssociationServiceSdkConfig) {

        this._diContainer = new DiContainer(config);

    }

    listPartnerRepAssociationsWithPartnerId(partnerId:string,
                                            accessToken:string):Promise<PartnerRepAssociationSynopsisView[]> {

        return this
            ._diContainer
            .get(ListPartnerRepAssociationsWithPartnerIdFeature)
            .execute(
                partnerId,
                accessToken);

    }

}
