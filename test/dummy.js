/**
 * dummy objects (see: http://xunitpatterns.com/Dummy%20Object.html)
 */
export default  {
    firstName: 'firstName',
    lastName: 'lastName',
    accountId:'000000000000000000',
    sapVendorNumber:'sapVendorNo',
    userId:'0000000000000',
    emailAddress:'email@test.com',
    url:'https://dummy-url.com',
    partnerRepAssociationId:'2342342332333'
};