import PartnerRepAssociationSynopsisView from '../../src/partnerRepAssociationSynopsisView';
import dummy from '../dummy';

describe('PartnerRepAssociationSynopsisView', ()=> {
    describe('constructor', () => {
        it('throws if id is null', () => {
            /*
             arrange
             */
            const constructor =
                () => new PartnerRepAssociationSynopsisView(
                    null,
                    dummy.accountId,
                    dummy.userId
                );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'id required');
        });
        it('sets id', () => {
            /*
             arrange
             */
            const expectedId = dummy.partnerRepAssociationId;

            /*
             act
             */
            const objectUnderTest =
                new PartnerRepAssociationSynopsisView(
                    expectedId,
                    dummy.accountId,
                    dummy.userId
                );

            /*
             assert
             */
            const actualId = objectUnderTest.id;

            expect(actualId).toEqual(expectedId);

        });
        it('throws if partnerId is null', () => {
            /*
             arrange
             */
            const constructor =
                () => new PartnerRepAssociationSynopsisView(
                    dummy.partnerRepAssociationId,
                    null,
                    dummy.userId
                );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'partnerId required');
        });

        it('sets partnerId', () => {
            /*
             arrange
             */
            const expectedPartnerId = dummy.accountId;

            /*
             act
             */
            const objectUnderTest =
                new PartnerRepAssociationSynopsisView(
                    dummy.partnerRepAssociationId,
                    expectedPartnerId,
                    dummy.userId
                );

            /*
             assert
             */
            const actualPartnerId = objectUnderTest.partnerId;

            expect(actualPartnerId).toEqual(expectedPartnerId);

        });
        it('throws if repId is null', () => {
            /*
             arrange
             */
            const constructor =
                () => new PartnerRepAssociationSynopsisView(
                    dummy.partnerRepAssociationId,
                    dummy.accountId,
                    null
                );

            /*
             act/assert
             */
            expect(constructor).toThrow();
        });

        it('sets repId', () => {
            /*
             arrange
             */
            const expectedRepId = dummy.userId;

            /*
             act
             */
            const objectUnderTest =
                new PartnerRepAssociationSynopsisView(
                    dummy.partnerRepAssociationId,
                    dummy.accountId,
                    expectedRepId
                );

            /*
             assert
             */
            const actualRepId = objectUnderTest.repId;

            expect(actualRepId).toEqual(expectedRepId);

        });

    })
});
