import PartnerRepAssociationServiceSdk from '../../src/index';
import PartnerRepServiceSdk,{AddPartnerRepReq} from 'partner-rep-service-sdk';
import config from './config';
import factory from './factory';
import dummy from '../dummy';

describe('Index module', () => {

    describe('default export', () => {
        it('should be PartnerRepAssociationServiceSdk constructor', () => {

            /*
             act
             */
            const objectUnderTest =
                new PartnerRepAssociationServiceSdk(config.partnerRepAssociationServiceSdkConfig);

            /*
             assert
             */
            expect(objectUnderTest).toEqual(jasmine.any(PartnerRepAssociationServiceSdk));

        });
    });

    describe('instance of default export', () => {

        describe('listPartnerRepAssociationsWithPartnerId method', () => {
            it('should return at least 1 result', (done) => {

                /*
                 arrange
                 */
                const objectUnderTest =
                    new PartnerRepAssociationServiceSdk(config.partnerRepAssociationServiceSdkConfig);

                // seed a partner rep association so we can retrieve it
                const partnerRepServiceSdk =
                    new PartnerRepServiceSdk(config.partnerRepServiceSdkConfig);

                const accountId =
                    config.accountIdOfExistingAccountWithAnSapAccountNumber;

                const arrangePromise =
                    partnerRepServiceSdk
                        .addPartnerRep(
                            new AddPartnerRepReq(
                                dummy.firstName,
                                dummy.lastName,
                                factory.constructUniqueEmailAddress(),
                                accountId
                            ),
                            factory.constructValidPartnerRepOAuth2AccessToken(
                                accountId
                            )
                        );

                /*
                 act
                 */
                const actPromise = arrangePromise
                    .then(
                        () =>
                            objectUnderTest
                                .listPartnerRepAssociationsWithPartnerId(
                                    accountId,
                                    factory.constructValidPartnerRepOAuth2AccessToken()
                                )
                    );

                /*
                 assert
                 */

                actPromise
                    .then(partnerRepAssociations => {
                        expect(partnerRepAssociations.length).toBeGreaterThan(1);
                        done();
                    })
                    .catch(error=> done.fail(JSON.stringify(error)));

            },30000)
        });
    });
});