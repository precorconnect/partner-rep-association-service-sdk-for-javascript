import PartnerRepAssociationServiceSdkConfig from '../../src/partnerRepAssociationServiceSdkConfig';
import {PartnerRepServiceSdkConfig} from 'partner-rep-service-sdk';

const precorConnectApiBaseUrl = 'https://api-dev.precorconnect.com';

export default {
    partnerRepAssociationServiceSdkConfig: new PartnerRepAssociationServiceSdkConfig(
        precorConnectApiBaseUrl
    ),
    partnerRepServiceSdkConfig: new PartnerRepServiceSdkConfig(
        precorConnectApiBaseUrl
    ),
    identityServiceJwtSigningKey: 'nbho9k9vcv8r48xGQs4woyN8BJ6q9X1efj295KXfS9A9yHJSRm0oU21j3ickrScQ',
    accountIdOfExistingAccountWithAnSapAccountNumber: '001K000001H2Km2IAF'
}
