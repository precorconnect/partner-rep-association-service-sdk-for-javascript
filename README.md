## Description
Precor Connect partner rep association service SDK for javascript.

## Features

##### List Partner Rep Associations With Partner Id
* [documentation](features/ListPartnerRepAssociationsWithPartnerId.feature)

## Setup

**install via jspm**  
```shell
jspm install partner-rep-association-service-sdk=bitbucket:precorconnect/partner-rep-association-service-sdk-for-javascript
``` 

**import & instantiate**
```javascript
import PartnerRepAssociationServiceSdk,{PartnerRepAssociationServiceSdkConfig} from 'partner-rep-association-service-sdk'

const partnerRepAssociationServiceSdkConfig = 
    new PartnerRepAssociationServiceSdkConfig(
        "https://api-dev.precorconnect.com"
    );
    
const partnerRepAssociationServiceSdk = 
    new PartnerRepAssociationServiceSdk(
        partnerRepAssociationServiceSdkConfig
    );
```

## Platform Support

This library can be used in the **browser**.

## Develop

#### Software
- git
- npm

#### Scripts

install dependencies (perform prior to running or testing locally)
```PowerShell
npm install
```

unit & integration test in multiple browsers/platforms
```PowerShell
# note: following environment variables must be present:
# SAUCE_USERNAME
# SAUCE_ACCESS_KEY
npm test
```